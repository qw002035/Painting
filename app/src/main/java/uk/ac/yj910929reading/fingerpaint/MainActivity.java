package uk.ac.yj910929reading.fingerpaint;



import android.graphics.Paint;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    /*
    this declares the variable names for the drawingview class, for the paint
    and the enum values for the color and sizes, as well as the variable names to reference
    the enums
     */
    DrawingView dv ;
    private Paint mPaint;
    public enum COLOR {RED, BLACK, GREEN};
    public enum SIZES {THIN, NORMAL, THICK}
    public COLOR brushColor;
    public SIZES brushSize;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        dv = (DrawingView)findViewById(R.id.drawingView);

       /*
       The following sets the buttons to their variable names
        */

        Button btnColorB = (Button)findViewById(R.id.buttonB);
        Button btnColorR = (Button)findViewById(R.id.buttonR);
        Button btnColorG = (Button)findViewById(R.id.buttonG);
        Button btnSizeT = (Button)findViewById(R.id.sizeT);
        Button btnSizeTh = (Button)findViewById(R.id.sizeTh);
        Button btnSizeN = (Button)findViewById(R.id.sizeN);

        /*
        This sets the appropriate strings to the buttons using the setText function
         */
        btnColorB.setText(R.string.ColourB);
        btnColorR.setText(R.string.ColourR);
        btnColorG.setText(R.string.ColourG);
        btnSizeT.setText(R.string.SizeT);
        btnSizeTh.setText(R.string.SizeTh);
        btnSizeN.setText(R.string.SizeN);


        // sets the initial colour and size for the brushes
        brushColor = COLOR.RED;
        brushSize = SIZES.NORMAL;

        /*
        the following calls the updatecolour/updatesize functions, in order to use them
        later for when the buttons are activated
         */

        updateBColor();
        updateRColor();
        updateGColor();
        updateThin();
        updateThick();
        updateNormal();



        /*
        This handles the button click for the black button, setting it to update the color to black
         */
        btnColorB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateBColor();
            }
        });

        /*
        This handles the button click for the red button, setting it to update the color to red
         */
        btnColorR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateRColor();
            }
        });

        /*
        This handles the button click for the green button, setting it to update the color to green
         */
        btnColorG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateGColor();
            }
        });

        /*
        This handles the button click for the Thin button, setting it to update the size of the
        strokes to thin
         */
        btnSizeT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateThin();
            }
        });

        /*
        This handles the button click for the normal button, setting it to update the size of the
        strokes to normal
         */

        btnSizeN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateNormal();
            }
        });

        /*
        This handles the button click for the thick button, setting it to update the size of the
        strokes to thick
         */

        btnSizeTh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateThick();
            }
        });

    }

    /*
    The update colour function, uses a switch to determine what colour the brush
    already is and then returns this colour to the setblackpaint function in the drawing view
    class in order to set the colour to black
     */

    public void updateBColor(){
        switch(brushColor){
            case RED:
                brushColor = COLOR.RED;
                break;
            case BLACK:
                brushColor = COLOR.BLACK;
                break;
            case GREEN:
                brushColor = COLOR.GREEN;
                break;

            default:
                break;
        }

        dv.setBPaintColor(brushColor);
    }

     /*
    The update colour function, uses a switch to determine what colour the brush
    already is and then returns this colour to the setredpaint function in the drawing view
    class in order to set the colour to red
     */

    public void updateRColor(){
        switch(brushColor){
            case RED:
                brushColor = COLOR.RED;
                break;
            case BLACK:
                brushColor = COLOR.BLACK;
                break;
            case GREEN:
                brushColor = COLOR.GREEN;
                break;

            default:
                break;
        }

        dv.setRPaintColor(brushColor);
    }

     /*
    The update colour function, uses a switch to determine what colour the brush
    already is and then returns this colour to the setgreenpaint function in the drawing view
    class in order to set the colour to green
     */

    public void updateGColor(){
        switch(brushColor){
            case RED:
                brushColor = COLOR.RED;
                break;
            case BLACK:
                brushColor = COLOR.BLACK;
                break;
            case GREEN:
                brushColor = COLOR.GREEN;
                break;

            default:
                break;
        }

        dv.setGPaintColor(brushColor);
    }


    /*
   The update thin function, uses a switch to determine what size the brush
   already is and then returns this size to the setthinsize function in the drawing view
   class in order to set the size to thin
    */
    public void updateThin(){

        switch(brushSize){
            case THIN:
                brushSize = SIZES.THIN;
                break;
            case NORMAL:
                brushSize = SIZES.THIN;
                break;
            case THICK:
                brushSize = SIZES.THIN;
                break;

            default:
                break;
        }

        dv.setThinsize(brushSize);
    }

    /*
   The update normal function, uses a switch to determine what size the brush
   already is and then returns this size to the setnormal function in the drawing view
   class in order to set the size to normal
    */
    public void updateNormal(){

        switch(brushSize){
            case THIN:
                brushSize = SIZES.NORMAL;
                break;
            case NORMAL:
                brushSize = SIZES.NORMAL;
                break;
            case THICK:
                brushSize = SIZES.NORMAL;
                break;

            default:
                break;
        }

        dv.setNormalsize(brushSize);
    }

    /*
   The update thick function, uses a switch to determine what size the brush
   already is and then returns this size to the setthicksize function in the drawing view
   class in order to set the size to thick
    */

    public void updateThick(){

        switch(brushSize){
            case THIN:
                brushSize = SIZES.THICK;
                break;
            case NORMAL:
                brushSize = SIZES.THICK;
                break;
            case THICK:
                brushSize = SIZES.THICK;
                break;

            default:
                break;
        }

        dv.setThicksize(brushSize);
    }




}
